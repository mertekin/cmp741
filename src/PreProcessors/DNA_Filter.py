
def FilterDNA(file_path: str):
    src_file = open(file_path)
    target_file = open("filtered_dna.txt", "w+")
    c = src_file.read(1)
    while c:
        if c in ['a', 't', 'g', 'c']:
            target_file.write(c)
        c = src_file.read(1)
    src_file.close()
    target_file.close()

if __name__ == '__main__':
    FilterDNA("hs_ref_GRCh38.p12_chr1.gbk")