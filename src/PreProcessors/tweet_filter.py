
def FilterTweet(file_path: str):
    src_file = open(file_path)
    target_file = open("filtered_tweet.txt", "w+")
    c = src_file.read(1)
    anormal_count = 0
    while c:
        if ord(c) < 256 :
            target_file.write(c)
        else:
            anormal_count += 1
        c = src_file.read(1)
    print("anormal count: " + str(anormal_count))
    src_file.close()
    target_file.close()

if __name__ == '__main__':
    FilterTweet("tweet_data.csv")