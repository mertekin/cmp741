def CreateMediumAndSmall(source_file_path: str, m_file_path: str, s_file_path: str):
    f = open(source_file_path)
    full_str = f.read()
    f.close()
    m_str = full_str[:int(len(full_str) / 2)]
    s_str = m_str[:int(len(full_str) / 4)]
    m_f = open(m_file_path, "w+")
    m_f.write(m_str)
    m_f.close()
    s_f = open(s_file_path, "w+")
    s_f.write(s_str)
    s_f.close()

def CreateLageMData(source_path: str, target_path: str,  query_path: str):
    f = open(source_path)
    full_str = f.read()
    f.close()
    target_str = full_str[:5000]
    q_str = target_str[:2000]
    t_f = open(target_path, "w+")
    t_f.write(target_str)
    t_f.close()
    q_f = open(query_path, "w+")
    q_f.write(q_str)
    q_f.close()

if __name__ == '__main__':
    CreateMediumAndSmall(
        "C:\\Users\\rtknm\\\Dropbox\\\MasterDersleri\\CMP741 Advanced Algorithms Analysis 1\\project\\src\Data\\Binary\\binary_data_m.txt",
        "C:\\Users\\rtknm\\\Dropbox\\\MasterDersleri\\CMP741 Advanced Algorithms Analysis 1\\project\\src\Data\\Binary\\binary_data.txt",
        "C:\\Users\\rtknm\\\Dropbox\\\MasterDersleri\\CMP741 Advanced Algorithms Analysis 1\\project\\src\Data\\Binary\\binary_data_s.txt"
    )
    print("Medium And Small Binary Data Created")
    CreateMediumAndSmall(
        "C:\\Users\\rtknm\\\Dropbox\\\MasterDersleri\\CMP741 Advanced Algorithms Analysis 1\\project\\src\Data\\DNA\\dna_data.txt",
        "C:\\Users\\rtknm\\\Dropbox\\\MasterDersleri\\CMP741 Advanced Algorithms Analysis 1\\project\\src\Data\\DNA\\dna_data_m.txt",
        "C:\\Users\\rtknm\\\Dropbox\\\MasterDersleri\\CMP741 Advanced Algorithms Analysis 1\\project\\src\Data\\DNA\\dna_data_s.txt"
    )

    print("Medium And Small DNA Data Created")
    CreateMediumAndSmall(
        "C:\\Users\\rtknm\\\Dropbox\\\MasterDersleri\\CMP741 Advanced Algorithms Analysis 1\\project\\src\Data\\Tweet\\tweet_data.txt",
        "C:\\Users\\rtknm\\\Dropbox\\\MasterDersleri\\CMP741 Advanced Algorithms Analysis 1\\project\\src\Data\\Tweet\\tweet_data_m.txt",
        "C:\\Users\\rtknm\\\Dropbox\\\MasterDersleri\\CMP741 Advanced Algorithms Analysis 1\\project\\src\Data\\Tweet\\tweet_data_s.txt"
    )
    print("Medium And Small Tweet Data Created")

    CreateLageMData(
        "C:\\Users\\rtknm\\\Dropbox\\\MasterDersleri\\CMP741 Advanced Algorithms Analysis 1\\project\\src\Data\\DNA\\dna_data.txt",
        "C:\\Users\\rtknm\\\Dropbox\\\MasterDersleri\\CMP741 Advanced Algorithms Analysis 1\\project\\src\Data\\DNA\\dna_data_large_m.txt",
        "C:\\Users\\rtknm\\\Dropbox\\\MasterDersleri\\CMP741 Advanced Algorithms Analysis 1\\project\\src\Data\\DNA\\dna_query_large_m.txt"
    )
    print("Large M DNA data created")