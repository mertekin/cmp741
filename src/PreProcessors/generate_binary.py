import random
def GenerateBinary(target: str, size: int):
    file = open(target, 'w+')
    old_rate  = 0.0
    rate = 0.0
    print("starting..")
    for i in range(size):
        file.write(str(random.randint(0, 1)))
        rate = i * 100.0 / (size * 1.0)
        if (rate - old_rate > 0.5):
            print("{0:.2f} %".format(rate))
            old_rate = rate
    file.close()
    print("{0:.2f} %".format(rate))
if __name__ == '__main__':
    GenerateBinary("binary_data.txt", 230000000)