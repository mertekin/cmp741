import time
from kmp import search as kmp_search
from rabin_krap import  search as rabin_search
from automaton_matcher import search as outomaton_search

test_results = {}
program_start_time = time.time()

def LoadData(file_path: str):
    f = open(file_path)
    txt = f.read()
    f.close()
    return txt

def LoadQueries(file_path: str):
    qf = open(file_path)
    queries = qf.readlines()
    qf.close()
    return queries

def ExecuteQueries(txt: str, queries = list(), test_name = ""):
    for query in queries:
        query = query.replace("\n", "")
        start_time = time.time()
        kmp_search(query, txt)
        end_time = time.time()
        print(test_name + " kmp query_len: " + str(len(query)) + " execution time:\t" + str(end_time - start_time))
        start_time = time.time()
        rabin_search(query, txt)
        end_time = time.time()
        print(test_name + " rabin query_len: " + str(len(query)) + " execution time:\t" + str(end_time - start_time))
        start_time = time.time()
        outomaton_search(query, txt)
        end_time = time.time()
        print(test_name + " auto query_len: " + str(len(query)) + " execution time:\t" + str(end_time - start_time))



#choose data to test
binary_enabled = False
dna_enabled = False
tweet_enabled = False

#choose data size to test
medium_enabled = False
small_enabled = False
large_enabled = False

enable_large_m = True

if enable_large_m:
    queries_binary = LoadQueries("Data/DNA/dna_query_large_m.txt")
    txt_binary = LoadData("Data/DNA/dna_data_large_m.txt")
    ExecuteQueries(txt_binary, queries_binary, "DNA large M")

if binary_enabled:
    queries_binary = LoadQueries("Data/Binary/binary_query.txt")
    if large_enabled:
        txt_binary = LoadData("Data/Binary/binary_data.txt")
        ExecuteQueries(txt_binary, queries_binary, "binary_L")
    if medium_enabled:
        txt_binary = LoadData("Data/Binary/binary_data_m.txt")
        ExecuteQueries(txt_binary, queries_binary, "binary_m")
    if small_enabled:
        txt_binary = LoadData("Data/Binary/binary_data_s.txt")
        ExecuteQueries(txt_binary, queries_binary, "binary_s")
if dna_enabled:
    queries_dna = LoadQueries("Data/DNA/dna_query.txt")
    if large_enabled:
        txt_dna = LoadData("Data/DNA/dna_data.txt")
        ExecuteQueries(txt_dna, queries_dna, "DNA_L")
    if medium_enabled:
        txt_dna = LoadData("Data/DNA/dna_data_m.txt")
        ExecuteQueries(txt_dna, queries_dna, "DNA_m")
    if small_enabled:
        txt_dna = LoadData("Data/DNA/dna_data_s.txt")
        ExecuteQueries(txt_dna, queries_dna, "DNA_s")

if tweet_enabled:
    queries_tweet = LoadQueries("Data/Tweet/tweet_query.txt")
    if large_enabled:
        txt_tweet = LoadData("Data/Tweet/tweet_data.txt")
        ExecuteQueries(txt_tweet, queries_tweet, "Tweet_L")
    if medium_enabled:
        txt_tweet = LoadData("Data/Tweet/tweet_data_m.txt")
        ExecuteQueries(txt_tweet, queries_tweet, "Tweet_m")
    if small_enabled:
        txt_tweet = LoadData("Data/Tweet/tweet_data_s.txt")
        ExecuteQueries(txt_tweet, queries_tweet, "Tweet_s")

print(test_results)

program_end_time = time.time()

print("total time = " + str(program_end_time - program_start_time))