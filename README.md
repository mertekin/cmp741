# CMP741

CMP741 Assignment - String Search Algorithms

This project created with pycharm 2018.3 IDE and python 3.7.1 compiler.
docs folder contains some referances and documents for project report.
venc folder contains some ide files.
src folder contains source codes and datas. But git system doens't able to hold  large data files properly so from the following dropbox link download the data you need or whole project.
https://www.dropbox.com/sh/ve82ev5ppnuqhim/AABbtQyKjIQeRNZ2v9VBwTPpa?dl=0

EXECUTING TESTS

Tests are executed from main.py script. In main.py there are some flags that descibes  which type of data to test and which sizes to test. main.py doen't except parameter can be run by ide or just typing python main.py. Don not forget to download datas before running main.py

HOW DATAS PREPARED
tweets: https://www.kaggle.com/kazanova/sentiment140#training.1600000.processed.noemoticon.csv
genome: ftp://ftp.ncbi.nih.gov/genomes/Homo_sapiens
binary: created with generate_binary.py
In src folder there is PreProcessors folder scripts in this folder are used to normalize, standardize original datas for the shake of simlicity.